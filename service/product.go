package service

import (
	"context"
	pb "temp/genproto/product"
	"temp/pkg/logger"
	"temp/storage"

	"github.com/jmoiron/sqlx"
	
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct{
	storage storage.IStorage
	logger logger.Logger
}

func NewProductService(db *sqlx.DB, log logger.Logger) *ProductService{
	return &ProductService{
		storage: storage.NewStoragePg(db),
		logger: log,
	}
}
func (s *ProductService) CreateType(ctx context.Context, req *pb.Type) (*pb.Type, error) {
	types, err := s.storage.Product().CreateType(req)
	if err != nil{
		s.logger.Error("Error with Type",logger.Any("error insert type",err))
		return &pb.Type{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return types, nil
}
//
func (s *ProductService) GetProduct(ctx context.Context, req *pb.GetProductRequest) (*pb.Prodct, error){
	product, err := s.storage.Product().GetProduct(req.ProductId)
	if err != nil{
		s.logger.Error("Error with Product",logger.Any("error insert Product",err))
		return &pb.Prodct{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	
	return product, nil
}

func (s *ProductService) CreateProduct(ctx context.Context, req *pb.Prodct) (*pb.Prodct, error){
	product, err := s.storage.Product().CreateProduct(req)
	if err != nil{
		s.logger.Error("Error with Product",logger.Any("error insert Product",err))
		return &pb.Prodct{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return product, nil
}

func (s *ProductService) GetUserProducts(ctx context.Context, req *pb.GetUserProductsRequest) (*pb.Products, error) {
	product, err := s.storage.Product().GetUserProducts(req.OwnerId)
	if err != nil{
		s.logger.Error("Error with Product",logger.Any("error insert Product",err))
		return &pb.Products{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return product, nil
}
//

func (s *ProductService) CreateCategory(ctx context.Context, req *pb.Category) (*pb.Category, error) {
	category, err := s.storage.Product().CreateCategory(req)
	if err != nil{
		s.logger.Error("Error with Category",logger.Any("error insert Category",err))
		return &pb.Category{}, status.Error(codes.Internal,"something went wrong, please check Category info")
	}
	return category, nil
}

func (s *ProductService) GetTypeCategorys(ctx context.Context, req *pb.GetTypeCategoryRequest) (*pb.Categorys, error) {
	category, err := s.storage.Product().GetTypeCategorys(req.TypeId)
	if err != nil{
		s.logger.Error("Error with Category",logger.Any("error insert Category",err))
		return &pb.Categorys{}, status.Error(codes.Internal,"something went wrong, please check Category info")
	}
	return category, nil
}