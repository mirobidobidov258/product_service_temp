package repo

import pb "temp/genproto/product"

type ProductStorageI interface{

	CreateType(*pb.Type) (*pb.Type, error)
	
	CreateProduct(*pb.Prodct) (*pb.Prodct, error);
	GetProduct(ID int64) (*pb.Prodct, error);
	GetUserProducts(ownerID int64) (*pb.Products, error);

	CreateCategory(*pb.Category) (*pb.Category, error);
	GetTypeCategorys(typeId int64) (*pb.Categorys, error);

}